export class Square {
    private color = 'red';
    private x = 0;
    private y = 0;
    private z = 30;
  
    constructor(private ctx: CanvasRenderingContext2D) {}
    
    //method to move the square right
    moveRight() {
      this.x++;
      this.draw();
    }
  
    /*method to draw
      a square filled with red color*/
      private draw() { 
      this.ctx.fillStyle = this.color;
      this.ctx.fillRect(this.z * this.x, this.z * this.y, this.z, this.z);
    }
  }
  