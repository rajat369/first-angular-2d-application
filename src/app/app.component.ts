import { Component, OnInit, ViewChild, ElementRef, NgZone, OnDestroy } from '@angular/core';
import { Square } from './Square';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
  
  //styles: ['canvas { border-style: solid }']
  
})
export class AppComponent implements OnInit,OnDestroy {

  @ViewChild('canvas',{static:true})  //passing the reference variable used in html file for canvas element 
  canvas:ElementRef<HTMLCanvasElement>;
   ctx: CanvasRenderingContext2D;
   requestId;
   interval;
   squares:Square[]=[];
   constructor(private ngZone:NgZone){}
  ngOnInit(): void {
    this.ctx=this.canvas.nativeElement.getContext('2d'); //for creating a canvas
    this.ctx.fillStyle='red';
    this.ngZone.runOutsideAngular(()=>this.tick()); //for ignoring change detection by Angular for tick function
    setInterval(()=>{  //for making the square appear to move
    this.tick();
  }
    ,200);
  }
  /*function for animation or movement of square*/
  tick(){
    this.ctx.clearRect(0,0,this.ctx.canvas.width,this.ctx.canvas.height); //for clearing rectangle to show mmovement
    this.squares.forEach((square:Square)=>{  //loop to pass multiple squares together 
      square.moveRight(); //for moving the square along x-axis to the right direction of canvas
    });
    this.requestId= requestAnimationFrame(()=>this.tick); //create an animation frame for callback function
  }

  play(){
    const square= new Square(this.ctx);   //creating a new square on click of play button
    this.squares=this.squares.concat(square); //pushing each square into array of squares

  }
  ngOnDestroy(){
    clearInterval(this.interval);
    cancelAnimationFrame(this.requestId); //destroy or cancel created animation frame on request
  }
}
